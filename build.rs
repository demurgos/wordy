use std::fs;
use std::io::ErrorKind;
use std::path::PathBuf;
use std::process::Command;

// The build script is only used locally, it is not part of the uploaded crate files
fn main() {
  let repo_dir = PathBuf::from(env!("CARGO_MANIFEST_DIR")).join("./");

  let repo_dir = match repo_dir.canonicalize() {
    Ok(rd) => rd,
    Err(e) => {
      eprintln!("ERR: failed to canonicalize repo dir {repo_dir:?}: {e:?}");
      std::process::exit(1);
    }
  };

  eprintln!("OK: repo_dir={repo_dir:?}");

  let package_dir = repo_dir.join("frontend");
  match fs::metadata(package_dir.as_path()) {
    Ok(m) if m.is_dir() => {
      eprintln!("OK: package directory exists at {package_dir:?}");
    }
    r => {
      eprintln!("ERR: invalid package directory at {package_dir:?}: {r:?}");
      std::process::exit(1);
    }
  }

  let browser_dir = package_dir.join("dist/frontend/browser");
  match fs::metadata(browser_dir.as_path()) {
    Ok(m) if m.is_dir() => {
      eprintln!("OK: browser directory exists at {browser_dir:?}");
    }
    Err(e) if e.kind() == ErrorKind::NotFound => {
      eprintln!("OK: browser directory will be built at {browser_dir:?}");
      {
        let yarn_out = Command::new("yarn")
          .args(["install"])
          .current_dir(package_dir.as_path())
          .output();
        let yarn_out = match yarn_out {
          Ok(out) => out,
          Err(e) => {
            eprintln!("ERR: `yarn install` failed: {e:?}");
            std::process::exit(1);
          }
        };
        let status = yarn_out.status;
        if !status.success() {
          eprintln!("ERR: `yarn install` had non-success exit status: {status:?}");
          std::process::exit(1);
        };
      }
      {
        let yarn_out = Command::new("yarn")
          .args(["run", "build"])
          .current_dir(package_dir.as_path())
          .output();
        let yarn_out = match yarn_out {
          Ok(out) => out,
          Err(e) => {
            eprintln!("ERR: `yarn run build` failed: {e:?}");
            std::process::exit(1);
          }
        };
        let status = yarn_out.status;
        if !status.success() {
          eprintln!("ERR: `yarn run build` had non-success exit status: {status:?}");
          std::process::exit(1);
        };
      }
      match fs::metadata(browser_dir.as_path()) {
        Ok(m) if m.is_dir() => {
          eprintln!("OK: browser directory exists at {browser_dir:?}");
        }
        r => {
          eprintln!("ERR: invalid browser directory at {browser_dir:?}: {r:?}");
          std::process::exit(1);
        }
      }
    }
    r => {
      eprintln!("ERR: invalid content at {browser_dir:?}: {r:?}");
      std::process::exit(1);
    }
  };
  println!("cargo:rerun-if-changed={}", browser_dir.display());
}
