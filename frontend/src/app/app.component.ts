import {Component} from '@angular/core';
import {RouterOutlet} from '@angular/router';
import {GameService, GameState, NetworkHealth} from "../game.mjs";
import {BehaviorSubject, Observable} from "rxjs";
import {CommonModule} from "@angular/common";
import {WordyUserComponent} from "./user.component";
import {WordyWordBoxComponent} from "./word-box.component";
import {CommandType, Phase} from "../protocol.mjs";

@Component({
  selector: 'app-root',
  standalone: true,
  imports: [RouterOutlet, CommonModule, WordyUserComponent, WordyWordBoxComponent],
  templateUrl: './app.component.html',
  styleUrl: './app.component.scss'
})
export class AppComponent {
  #game: GameService;
  public networkHealth$: BehaviorSubject<NetworkHealth>;
  public state$: Observable<GameState>;
  public Phase = Phase;

  constructor(game: GameService) {
    this.#game = game;
    this.networkHealth$ = game.networkHealth$;
    this.state$ = game.state$;
  }

  resetChill() {
    this.#game.submit({
      type: CommandType.ResetPhase,
      phase: Phase.Chill,
    });
  }

  resetHint() {
    this.#game.submit({
      type: CommandType.ResetPhase,
      phase: Phase.Hint,
    });
  }

  resetCheck() {
    this.#game.submit({
      type: CommandType.ResetPhase,
      phase: Phase.Check,
    });
  }

  resetGuess() {
    this.#game.submit({
      type: CommandType.ResetPhase,
      phase: Phase.Guess,
    });
  }

  startHint() {
    this.#game.submit({
      type: CommandType.NextPhase,
      old_phase: Phase.Chill,
    });
  }

  startCheck() {
    this.#game.submit({
      type: CommandType.NextPhase,
      old_phase: Phase.Hint,
    });
  }

  startGuess() {
    this.#game.submit({
      type: CommandType.NextPhase,
      old_phase: Phase.Check,
    });
  }

  startChill() {
    this.#game.submit({
      type: CommandType.NextPhase,
      old_phase: Phase.Guess,
    });
  }

  bumpWin() {
    this.#game.submit({
      type: CommandType.BumpWin,
    });
  }

  bumpSkip() {
    this.#game.submit({
      type: CommandType.BumpSkip,
    });
  }

  bumpLoss() {
    this.#game.submit({
      type: CommandType.BumpLoss,
    });
  }

  resetScore() {
    this.#game.submit({
      type: CommandType.ResetScore,
    });
  }

  sync() {
    this.#game.submit({
      type: CommandType.Sync,
    });
  }

  isAdmin(state: GameState): boolean {
    for (const u of state.users) {
      if (u.id === state.me.id && u.isAdmin) {
        return true;
      }
    }
    return false;
  }
}
