import {Component, Input} from '@angular/core';
import {RouterOutlet} from '@angular/router';
import {GameService, GameState, GameUser, UserRole} from "../game.mjs";
import {CommonModule} from "@angular/common";
import {WordyWordBoxComponent} from "./word-box.component";
import {CommandType, Phase} from "../protocol.mjs";

@Component({
  selector: 'wordy-user',
  standalone: true,
  imports: [RouterOutlet, CommonModule, WordyWordBoxComponent],
  template: `
    <div class="card {{user.role}}">
      <div class="actions">
        @if (state.me.id === user.id) {
          <span class="sbutton edit" (click)="rename()"></span>
        }
      </div>
      <div class="name">{{ user.displayName }}</div>
      <div class="image"><img src="{{user.image}}"></div>
      <div class="word">
        <wordy-word-box [word]="user.word" [hasWord]="user.hasWord" [write]="isWritable(state, user) ? user.id : null"/>
        @if (canVote(state, user)) {
          <!--          <span class="sbutton down" (click)="vote(user.id, false)"></span>-->
          <span class="sbutton strike" (click)="vote(user.id, true)"></span>
        }
      </div>
    </div>
  `,
  styleUrl: './app.component.scss'
})
export class WordyUserComponent {
  @Input("user")
  public user!: GameUser;
  @Input("state")
  public state!: GameState;

  #game: GameService;

  public constructor(game: GameService) {
    this.#game = game;
  }

  public isWritable(state: GameState, user: GameUser): boolean {
    switch (state.phase) {
      case Phase.Chill:
        return false;
      case Phase.Hint:
        return user.role === UserRole.Hinter && user.id === state.me.id && !user.hasWord;
      case Phase.Check:
        return false;
      case Phase.Guess:
        return user.role === UserRole.Guesser && user.id === state.me.id && !user.hasWord;
    }
  }

  public canVote(state: GameState, user: GameUser): boolean {
    let me = state.getMe();
    return me.role === UserRole.Hinter && user.hasWord && user.role === UserRole.Hinter && state.phase === Phase.Check;
  }

  public vote(userId: string, isUp: boolean) {
    this.#game.submit({type: CommandType.UnsetWord, user: userId});
  }

  rename() {
    let newName = prompt("New name?");
    if ((typeof newName === "string") && newName.length > 0 && newName.length < 16) {
      this.#game.submit({
        type: CommandType.Rename,
        new_name: newName,
      });
    }
  }
}
