import {Component, Input} from '@angular/core';
import {RouterOutlet} from '@angular/router';
import {CommonModule} from "@angular/common";
import {FormControl, FormGroup, ReactiveFormsModule, Validators} from "@angular/forms";
import {GameService} from "../game.mjs";
import {CommandType} from "../protocol.mjs";

@Component({
  selector: 'wordy-word-box',
  standalone: true,
  imports: [RouterOutlet, CommonModule, ReactiveFormsModule],
  template: `
    @if (word !== null && write === null) {
      <span>{{ word }}</span>
    } @else if (write !== null) {
      <form (submit)="sendWord($event)" [formGroup]="newWordForm">
        <input type="text" [formControl]="newWord" value=""/>
        <input type="submit" name="newWordSend" [disabled]="newWord.invalid" value="Send"/>
      </form>
    } @else {
      @if (hasWord) {
        ???
      } @else {
        -
      }
    }
  `,
  styleUrl: './app.component.scss'
})
export class WordyWordBoxComponent {
  @Input("word")
  public word: string | null = null;
  @Input("hasWord")
  public hasWord: boolean = false;
  @Input("write")
  public write: string | null = null;

  public readonly newWord: FormControl;
  public readonly newWordForm: FormGroup;

  #game: GameService

  constructor(game: GameService) {
    this.newWord = new FormControl(
      "",
      [Validators.required, Validators.min(1), Validators.max(20)],
    );
    this.newWordForm = new FormGroup({
      newWord: this.newWord,
    });
    this.#game = game;
  }

  public sendWord(event: Event) {
    event.preventDefault();
    const model: any = this.newWordForm.getRawValue();
    console.log("HINTING");
    console.log(model.newWord);
    this.#game.submit({
      type: CommandType.SetWord,
      new_word: model.newWord,
    })
  }
}
