import {Config} from "./config-type.mjs";

export function getConfig(): Config {
  const url = new URL(document.location.href);
  url.protocol = ["https", "https:"].indexOf(url.protocol) >= 0 ? "wss" : "ws";
  url.pathname = "/game";

  return {
    gameEndpoint: url,
  }
}
