import {Config} from "./config-type.mjs";

export function getConfig(): Config {
  return {
    gameEndpoint: new URL("ws://localhost:2024/game"),
  }
}
