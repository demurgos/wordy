import {Injectable} from "@angular/core";
import {BehaviorSubject, Observable, Subject,} from "rxjs";
import {WebSocketSubject} from "rxjs/webSocket";
import {EventState, EventType, Phase, RawUser, WordEvent, WordyCommand} from "./protocol.mjs";
import {getConfig} from "./config/config.mjs";

// import {WebSocketMessage} from "rxjs/dist/types/internal/observable/dom/WebSocketSubject";

export enum NetworkHealth {
  Pending = "Pending",
  Healthy = "Healthy",
  Disconnected = "Disconnected",
}

export enum UserRole {
  Guesser = "Guesser",
  Hinter = "Hinter",
  Watcher = "Watcher",
}

export class GameState {
  public me: GameUser;
  public phase: Phase;
  public winCount: number;
  public lossCount: number;
  public skipCount: number;
  public target: string | null;
  public users: GameUser[];

  constructor(me: RawUser) {
    this.me = readUser(me);
    this.phase = Phase.Chill;
    this.target = null;
    this.users = [this.me];
    this.winCount = 0;
    this.winCount = 0;
    this.lossCount = 0;
    this.skipCount = 0;
    console.log(this.me);
  }

  getMe(): GameUser {
    for (const u of this.users) {
      if (u.id === this.me.id) {
        return u;
      }
    }
    return this.me;
  }

  handle(ev: WordEvent) {
    switch (ev.type) {
      case EventType.State:
        return this.handleState(ev);
      default:
        console.error(ev);
    }
  }

  handleState(ev: EventState) {
    this.phase = ev.state.phase;
    this.target = ev.state.target;
    this.winCount = ev.state.win_count,
      this.lossCount = ev.state.loss_count,
      this.skipCount = ev.state.skip_count,
      this.users = ev.state.player_queue.map(readUser);
  }
}

function readUser(raw: RawUser): GameUser {
  return {
    id: raw.id,
    role: raw.role,
    displayName: raw.display_name,
    image: raw.image_url ?? "/assets/user.png",
    word: raw.word,
    hasWord: raw.has_word,
    isAdmin: raw.is_admin,
    hearts: 0
  }
}

export interface GameUser {
  id: string;
  role: UserRole;
  displayName: string;
  image: string;
  word: null | string;
  hasWord: boolean;
  hearts: number;
  isAdmin: boolean;
}

@Injectable({providedIn: "root"})
export class GameService {
  public networkHealth$: BehaviorSubject<NetworkHealth>
  public state$: Observable<GameState>
  #ws$: WebSocketSubject<WordEvent>;

  constructor() {
    const networkHealth$ = new BehaviorSubject(NetworkHealth.Pending as NetworkHealth);
    let config = getConfig();
    const ws$: WebSocketSubject<WordEvent> = new WebSocketSubject({url: config.gameEndpoint.toString()});
    this.networkHealth$ = networkHealth$;
    const state$: Subject<GameState> = new Subject();
    let state: GameState | null = null;
    this.state$ = state$;
    const sub = ws$.subscribe({
      next(ev: WordEvent) {
        console.info(ev);
        if (networkHealth$.value === NetworkHealth.Pending) {
          if (ev.type === EventType.UserCreated) {
            networkHealth$.next(NetworkHealth.Healthy);
            state = new GameState(ev.user);
            state$.next(state);
          } else {
            networkHealth$.next(NetworkHealth.Disconnected);
            state = null;
            sub.unsubscribe();
          }
        } else if (networkHealth$.value === NetworkHealth.Healthy && state !== null) {
          state.handle(ev);
        } else {
          networkHealth$.next(NetworkHealth.Disconnected);
          state = null;
          sub.unsubscribe();
        }
      },
      error(err) {
        console.error(err);
        networkHealth$.next(NetworkHealth.Disconnected);
        state = null;
        sub.unsubscribe();
      },
      complete() {
        console.info("DISCONNECTED");
        networkHealth$.next(NetworkHealth.Disconnected);
        state = null;
        sub.unsubscribe();
      }
    });
    this.#ws$ = ws$;
  }

  public submit(command: WordyCommand) {
    this.#ws$.next(command as any);
  }
}
