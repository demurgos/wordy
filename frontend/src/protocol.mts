import {UserRole} from "./game.mjs";

export enum EventType {
  UserCreated = "UserCreated",
  State = "State",
}

export enum Phase {
  Chill = "Chill",
  Hint = "Hint",
  Check = "Check",
  Guess = "Guess",
}

export interface RawUser {
  id: string;
  display_name: string;
  image_url: string | null;
  is_admin: boolean;
  last_message: string;
  role: UserRole;
  word: string | null;
  has_word: boolean;
  win_count: number;
  loss_count: number;
  skip_count: number;
}

export interface EventUserCreated {
  type: EventType.UserCreated;
  user: RawUser;
}

export interface EventState {
  type: EventType.State;
  state: RawPartialGameState;
}

export interface RawPartialGameState {
  phase: Phase,
  target: string | null,
  player_queue: RawUser[],
  win_count: number,
  loss_count: number,
  skip_count: number,
}

export type WordEvent = EventUserCreated | EventState;

export enum CommandType {
  NextPhase = "NextPhase",
  ResetPhase = "ResetPhase",
  Rename = "Rename",
  SetWord = "SetWord",
  UnsetWord = "UnsetWord",
  BumpWin = "BumpWin",
  BumpLoss = "BumpLoss",
  BumpSkip = "BumpSkip",
  ResetScore = "ResetScore",
  Sync = "Sync",
}

export interface CommandRename {
  type: CommandType.Rename;
  new_name: string;
}

export interface CommandNextPhase {
  type: CommandType.NextPhase;
  old_phase: Phase;
}

export interface CommandResetPhase {
  type: CommandType.ResetPhase;
  phase: Phase;
}

export interface CommandSetWord {
  type: CommandType.SetWord;
  new_word: string;
}

export interface CommandUnsetWord {
  type: CommandType.UnsetWord;
  user: string;
}

export interface CommandBumpWin {
  type: CommandType.BumpWin;
}

export interface CommandBumpLoss {
  type: CommandType.BumpLoss;
}

export interface CommandBumpSkip {
  type: CommandType.BumpSkip;
}

export interface CommandResetScore {
  type: CommandType.ResetScore;
}

export interface CommandSync {
  type: CommandType.Sync;
}

export type WordyCommand =
  CommandNextPhase
  | CommandResetPhase
  | CommandRename
  | CommandSetWord
  | CommandUnsetWord
  | CommandBumpSkip
  | CommandBumpWin
  | CommandBumpLoss
  | CommandResetScore
  | CommandSync;
