use crate::declare_new_uuid;
use crate::image_list::ImageList;
use crate::types::DisplayErrorChain;
use crate::word_list::WordList;
use chrono::{DateTime, Utc};
use compact_str::CompactString;
use core::fmt;
use serde::{Deserialize, Serialize};
use std::collections::{BTreeMap, HashSet, VecDeque};
use tokio::sync::{mpsc, RwLock};
use url::Url;

fn name_to_img(name: &str) -> Option<&'static str> {
  match name.to_ascii_lowercase().as_str() {
    "demurgos" => Some("https://i.imgur.com/lrmbJEC.png"),
    "nassimou" => Some("https://i.imgur.com/osycgCe.png"),
    _ => None,
  }
}

#[derive(Debug, Clone, PartialEq, Eq, Serialize, Deserialize)]
#[serde(tag = "type")]
pub enum Command {
  Rename(CommandRename),
  NextPhase(CommandNextPhase),
  ResetPhase(CommandResetPhase),
  SetWord(CommandSetWord),
  UnsetWord(CommandUnsetWord),
  BumpWin(CommandBumpWin),
  BumpLoss(CommandBumpLoss),
  BumpSkip(CommandBumpSkip),
  ResetScore(CommandResetScore),
  Sync(CommandSync),
}

#[derive(Debug, Clone, PartialEq, Eq, Serialize, Deserialize)]
pub struct CommandRename {
  pub new_name: CompactString,
}

#[derive(Debug, Clone, PartialEq, Eq, Serialize, Deserialize)]
pub struct CommandNextPhase {
  pub old_phase: GamePhase,
}

#[derive(Debug, Clone, PartialEq, Eq, Serialize, Deserialize)]
pub struct CommandSetWord {
  pub new_word: CompactString,
}

#[derive(Debug, Clone, PartialEq, Eq, Serialize, Deserialize)]
pub struct CommandUnsetWord {
  pub user: UserId,
}

#[derive(Debug, Clone, PartialEq, Eq, Serialize, Deserialize)]
pub struct CommandBumpWin {}

#[derive(Debug, Clone, PartialEq, Eq, Serialize, Deserialize)]
pub struct CommandBumpLoss {}

#[derive(Debug, Clone, PartialEq, Eq, Serialize, Deserialize)]
pub struct CommandBumpSkip {}

#[derive(Debug, Clone, PartialEq, Eq, Serialize, Deserialize)]
pub struct CommandResetScore {}

#[derive(Debug, Clone, PartialEq, Eq, Serialize, Deserialize)]
pub struct CommandSync {}

#[derive(Debug, Clone, PartialEq, Eq, Serialize, Deserialize)]
pub struct CommandResetPhase {
  pub phase: GamePhase,
}

#[derive(Debug, Clone, PartialEq, Eq, Serialize, Deserialize)]
#[serde(tag = "type")]
pub enum Event {
  UserCreated(EventUserCreated),
  State(EventState),
}

#[derive(Debug, Clone, PartialEq, Eq, Serialize, Deserialize)]
pub struct EventUserCreated {
  user: User,
}

#[derive(Debug, Clone, PartialEq, Eq, Serialize, Deserialize)]
pub struct EventState {
  state: PartialGameState,
}

declare_new_uuid! {
  pub struct UserId(Uuid);
  pub type ParseError = UserIdParseError;
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Serialize, Deserialize)]
pub enum GamePhase {
  Chill,
  Hint,
  Check,
  Guess,
}

#[derive(Debug, Clone, Copy, PartialEq, Eq, Serialize, Deserialize)]
pub enum UserRole {
  Guesser,
  Hinter,
  Watcher,
}

#[derive(Debug, Clone, PartialEq, Eq, Serialize, Deserialize)]
pub struct User {
  id: UserId,
  display_name: CompactString,
  image_url: Option<Url>,
  is_admin: bool,
  last_message: DateTime<Utc>,
  role: UserRole,
  word: Option<CompactString>,
  has_word: bool,
  is_discarded_word: bool,
}

#[derive(Debug)]
pub struct GameState {
  pub word_list: WordList,
  pub image_list: ImageList,
  pub clock: u64,
  pub phase: GamePhase,
  pub target: Option<CompactString>,
  pub users: BTreeMap<UserId, User>,
  pub player_queue: VecDeque<UserId>,
  pub outbox: GameOutbox,
  pub win_count: u64,
  pub loss_count: u64,
  pub skip_count: u64,
}

pub struct GameOutbox {
  channels: BTreeMap<UserId, mpsc::UnboundedSender<Event>>,
}

impl GameOutbox {
  pub fn new() -> Self {
    Self {
      channels: BTreeMap::new(),
    }
  }
}

impl fmt::Debug for GameOutbox {
  fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
    write!(f, "GameOutbox {{")?;
    for (i, user) in self.channels.keys().enumerate() {
      if i > 0 {
        write!(f, ", ")?;
      }
      write!(f, "{user}")?;
    }
    write!(f, "}}")?;
    Ok(())
  }
}

#[derive(Debug, Clone, PartialEq, Eq, Serialize, Deserialize)]
pub struct PartialGameState {
  pub phase: GamePhase,
  pub target: Option<CompactString>,
  pub player_queue: Vec<User>,
  pub win_count: u64,
  pub loss_count: u64,
  pub skip_count: u64,
}

impl GameState {
  pub fn new() -> Self {
    Self {
      word_list: WordList::embedded(),
      image_list: ImageList::embedded(),
      clock: 0,
      phase: GamePhase::Chill,
      target: None,
      users: BTreeMap::new(),
      player_queue: VecDeque::new(),
      outbox: GameOutbox::new(),
      win_count: 0,
      loss_count: 0,
      skip_count: 0,
    }
  }
}

impl GameState {
  pub fn create_user(&mut self, user_id: UserId, channel: mpsc::UnboundedSender<Event>) {
    let display_name = user_id.to_hex();
    let display_name = format!("user{}", &display_name[0..8]);
    let user = User {
      id: user_id,
      display_name: CompactString::new(display_name),
      image_url: Some(self.image_list.pick()),
      is_admin: false,
      last_message: Utc::now(),
      role: UserRole::Watcher,
      word: None,
      has_word: false,
      is_discarded_word: false,
    };
    self.users.insert(user_id, user.clone());
    self.player_queue.push_back(user_id);
    self.outbox.channels.insert(user_id, channel);
    self.notify_one(user_id, Event::UserCreated(EventUserCreated { user }));
    self.sync();
  }

  pub fn handle(&mut self, actor: UserId, cmd: Command) {
    let time = self.bump_clock();
    println!("<--- {actor} {time:>03} {cmd:?}");
    match cmd {
      Command::Rename(cmd) => {
        let name = cmd.new_name.trim();
        if name.len() == 0 || name.len() > 16 {
          return;
        }
        let valid = name.chars().all(|c| matches!(c, 'A'..='Z' | 'a'..='z' | '0'..='9'));
        if !valid {
          return;
        }
        let user = match self.users.get_mut(&actor) {
          Some(u) => u,
          None => return,
        };
        let old_image = name_to_img(&user.display_name).map(|img| Url::parse(img).unwrap());
        let new_image = name_to_img(&name).map(|img| Url::parse(img).unwrap());
        match new_image {
          Some(img) => user.image_url = Some(img),
          None => {
            if old_image.is_some() {
              user.image_url = Some(self.image_list.pick())
            }
          }
        }
        user.display_name = CompactString::from(name);
        user.is_admin = name == "demurgos";
      }
      Command::NextPhase(cmd) => {
        let user = match self.users.get_mut(&actor) {
          Some(u) => u,
          None => return,
        };
        if !user.is_admin {
          return;
        }
        if cmd.old_phase != self.phase {
          dbg!("wrong phase");
          dbg!(cmd.old_phase);
          dbg!(self.phase);
          return;
        }
        match self.phase {
          GamePhase::Chill => self.start_hint(),
          GamePhase::Hint => self.start_check(),
          GamePhase::Check => self.start_guess(),
          GamePhase::Guess => self.start_chill(),
        }
      }
      Command::ResetPhase(cmd) => {
        let user = match self.users.get_mut(&actor) {
          Some(u) => u,
          None => return,
        };
        if !user.is_admin {
          return;
        }
        if cmd.phase != self.phase {
          return;
        }
        match self.phase {
          GamePhase::Chill => self.start_chill(),
          GamePhase::Hint => self.start_hint(),
          GamePhase::Check => self.start_check(),
          GamePhase::Guess => self.start_guess(),
        }
      }
      Command::SetWord(cmd) => {
        let user = match self.users.get_mut(&actor) {
          Some(u) => u,
          None => return,
        };
        let word = match get_word(cmd.new_word) {
          Some(word) => word,
          None => return,
        };
        match self.phase {
          GamePhase::Chill => return,
          GamePhase::Hint => {
            if user.role != UserRole::Hinter || user.has_word {
              return;
            }
            user.word = Some(word);
            user.has_word = true;
            let mut is_missing_hinter: bool = false;
            for u in self.users.values() {
              if u.role == UserRole::Hinter && !u.has_word {
                is_missing_hinter = true;
                break;
              }
            }
            if !is_missing_hinter {
              self.start_check();
            }
          }
          GamePhase::Check => return,
          GamePhase::Guess => {
            if user.role != UserRole::Guesser || user.has_word {
              return;
            }
            user.word = Some(word);
            user.has_word = true;
            self.start_chill();
          }
        }
      }
      Command::UnsetWord(cmd) => match self.phase {
        GamePhase::Check => {
          {
            let user = match self.users.get_mut(&actor) {
              Some(u) => u,
              None => return,
            };
            if user.role != UserRole::Hinter {
              return;
            }
          }
          let target = match self.users.get_mut(&cmd.user) {
            Some(target) => target,
            None => return,
          };
          if target.role != UserRole::Hinter {
            return;
          }
          target.word = None;
          target.has_word = false;
        }
        _ => return,
      },
      Command::BumpWin(_) => {
        let user = match self.users.get_mut(&actor) {
          Some(u) => u,
          None => return,
        };
        if !user.is_admin {
          return;
        }
        self.win_count += 1;
      }
      Command::BumpLoss(_) => {
        let user = match self.users.get_mut(&actor) {
          Some(u) => u,
          None => return,
        };
        if !user.is_admin {
          return;
        }
        self.loss_count += 1;
      }
      Command::BumpSkip(_) => {
        let user = match self.users.get_mut(&actor) {
          Some(u) => u,
          None => return,
        };
        if !user.is_admin {
          return;
        }
        self.skip_count += 1;
      }
      Command::ResetScore(_) => {
        let user = match self.users.get_mut(&actor) {
          Some(u) => u,
          None => return,
        };
        if !user.is_admin {
          return;
        }
        self.win_count = 0;
        self.loss_count = 0;
        self.skip_count = 0;
      }
      Command::Sync(_) => {
        let user = match self.users.get_mut(&actor) {
          Some(u) => u,
          None => return,
        };
        if !user.is_admin {
          return;
        }
        // sync is implicit
      }
    }
    self.sync();
  }

  fn start_hint(&mut self) {
    let guesser = match self.player_queue.front() {
      Some(g) => *g,
      None => return,
    };
    self.phase = GamePhase::Hint;
    self.target = Some(self.word_list.pick());
    for user in self.users.values_mut() {
      user.role = if user.id == guesser {
        UserRole::Guesser
      } else {
        UserRole::Hinter
      };
      user.word = None;
      user.has_word = false;
    }
  }

  fn start_check(&mut self) {
    self.phase = GamePhase::Check;
  }

  fn start_guess(&mut self) {
    self.phase = GamePhase::Guess;
  }

  fn start_chill(&mut self) {
    self.phase = GamePhase::Chill;
    let first = self.player_queue.pop_front();
    if let Some(first) = first {
      self.player_queue.push_back(first)
    }
  }

  fn sync(&mut self) {
    let mut dirty = true;
    while dirty {
      dirty = false;
      let mut dead_users: HashSet<UserId> = HashSet::new();
      for user in self.users.keys() {
        if !self.outbox.channels.contains_key(user) {
          dead_users.insert(*user);
        }
      }
      if !dead_users.is_empty() {
        dirty = true;
        self.player_queue.retain(|u| !dead_users.contains(u));
        for dead in dead_users {
          self.users.remove(&dead);
        }
      }
      let old_ob_count = self.outbox.channels.len();
      self.notify_state_all();
      if self.outbox.channels.len() != old_ob_count {
        dirty = true;
      }
    }
  }

  fn partial_for_user(&self, me: &User) -> PartialGameState {
    // let me = self.users.get(&user_id).expect("missing user");
    let list = self.collect_user_list_for_partial(me);
    PartialGameState {
      phase: self.phase,
      target: match self.target.as_ref() {
        None => None,
        Some(target) => {
          let can_see = match self.phase {
            GamePhase::Chill => true,
            GamePhase::Hint => me.role != UserRole::Guesser,
            GamePhase::Check => me.role != UserRole::Guesser,
            GamePhase::Guess => me.role != UserRole::Guesser,
          };
          if can_see {
            Some(target.clone())
          } else {
            None
          }
        }
      },
      player_queue: list,
      win_count: self.win_count,
      loss_count: self.loss_count,
      skip_count: self.skip_count,
    }
  }

  fn collect_user_list_for_partial(&self, me: &User) -> Vec<User> {
    let mut list: Vec<User> = Vec::new();
    for uid in &self.player_queue {
      let user = self.users.get(&uid).expect("crash");
      let mut user = user.clone();
      let can_see = (*uid == me.id)
        || match self.phase {
          GamePhase::Chill => true,
          GamePhase::Hint => me.role == UserRole::Watcher,
          GamePhase::Check => me.role != UserRole::Guesser,
          GamePhase::Guess => true,
        };
      user.word = match user.word {
        Some(w) if can_see => Some(w),
        _ => None,
      };
      list.push(user);
    }
    list
  }

  fn notify_state_all(&mut self) {
    let users = Vec::from_iter(self.users.values().cloned());
    for user in users {
      let state = self.partial_for_user(&user);
      self.notify_one(user.id, Event::State(EventState { state }));
    }
  }

  fn notify_one(&mut self, user_id: UserId, event: Event) {
    let time = self.bump_clock();
    if let Some(ob) = self.outbox.channels.get(&user_id) {
      println!("---> {user_id} {time:>03} {event:?}");
      if let Err(e) = ob.send(event) {
        println!("#### {user_id} notify error: {}", DisplayErrorChain(&e));
        self.outbox.channels.remove(&user_id);
      }
    }
  }

  // fn notify_all(&mut self, event: Event) {
  //   let mut users_to_remove: HashSet<UserId> = HashSet::new();
  //   let time = self.bump_clock();
  //   for (user_id, ob) in &self.outbox.channels {
  //     println!("---> {user_id} {time:>03} {event:?}");
  //     if let Err(e) = ob.send(event.clone()) {
  //       println!("#### {user_id} notify error: {}", DisplayErrorChain(&e));
  //       users_to_remove.insert(*user_id);
  //     }
  //   }
  //   self.player_queue.retain(|u| !users_to_remove.contains(&u));
  //   let need_refresh = !users_to_remove.is_empty();
  //   for user_id in users_to_remove {
  //     self.users.remove(&user_id);
  //     self.outbox.channels.remove(&user_id);
  //   }
  //   if need_refresh {
  //     self.send_user_list();
  //   }
  // }

  fn bump_clock(&mut self) -> u64 {
    let t = self.clock;
    self.clock += 1;
    t
  }
}

fn get_word(word: CompactString) -> Option<CompactString> {
  let word = word.trim();
  if word.len() == 0 || word.len() > 20 {
    return None;
  }
  // let valid = name.chars().all(|c| matches!(c, 'A'..='Z' | 'a'..='z' | '0'..='9'));
  // if !valid {
  //   return None;
  // }
  return Some(CompactString::new(word));
}

#[derive(Debug)]
pub struct Game {
  inner: RwLock<GameState>,
}

impl Game {
  pub fn new() -> Self {
    Self {
      inner: RwLock::new(GameState::new()),
    }
  }

  pub async fn create_user(&self, user_id: UserId, channel: mpsc::UnboundedSender<Event>) {
    self.inner.write().await.create_user(user_id, channel)
  }

  pub async fn handle(&self, actor: UserId, cmd: Command) {
    self.inner.write().await.handle(actor, cmd)
  }
}
