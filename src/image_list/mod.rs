use rand::rngs::ThreadRng;
use url::Url;

#[derive(Debug)]
pub struct ImageList {
  list: Vec<Url>,
}

impl ImageList {
  pub fn embedded() -> Self {
    let list = include_str!("./list.txt");
    let list = Vec::from_iter(
      list
        .split('\n')
        .filter(|s| !s.trim().is_empty())
        .map(|u| Url::parse(u).unwrap()),
    );
    Self { list }
  }

  pub fn pick(&self) -> Url {
    use rand::seq::SliceRandom;
    let mut rng = ThreadRng::default();
    self.list.as_slice().choose(&mut rng).unwrap().clone()
  }
}
