extern crate core;

use crate::game::Game;
use crate::router::router;
use crate::system::WordSystem;
use std::net::{SocketAddr, SocketAddrV6};
use std::sync::Arc;
use tokio::net::TcpListener;

pub mod game;
#[macro_use]
pub mod types;
pub mod image_list;
pub mod router;
pub mod system;
pub mod word_list;

#[tokio::main]
pub async fn main() {
  let game = Game::new();
  let system = WordSystem { game: Arc::new(game) };
  let router = router()
    .with_state(system)
    .into_make_service_with_connect_info::<SocketAddr>();
  let port = 2024;
  let socket_addr = SocketAddr::V6(SocketAddrV6::new("::".parse().unwrap(), port, 0, 0));
  let listener = TcpListener::bind(socket_addr).await.expect("failed to bind socket");
  eprintln!("server ready at http://localhost:{port}/");
  axum::serve(listener, router).await.expect("server crash");
}
