use crate::game::{Command, Event, UserId};
use crate::system::WordSystem;
use crate::types::DisplayErrorChain;
use axum::body::Body;
use axum::extract::ws::{Message, WebSocket};
use axum::extract::{ConnectInfo, State, WebSocketUpgrade};
use axum::http::{header, Request, Response, StatusCode, Uri};
use axum::middleware::Next;
use axum::response::IntoResponse;
use axum::routing::get;
use axum::Router;
use axum_extra::{headers, TypedHeader};
use compact_str::{CompactString, ToCompactString};
use futures::SinkExt;
use futures::StreamExt;
use include_dir::{include_dir, Dir};
use std::net::SocketAddr;
use tokio::sync::mpsc;
use tokio::task::JoinHandle;
use uuid::Uuid;

pub fn router() -> Router<WordSystem> {
  Router::new()
    .route("/game", get(ws_handler))
    .fallback(fallback)
    .layer(axum::middleware::from_fn(on_request))
}

async fn on_request(request: Request<Body>, next: Next) -> Response<Body> {
  let method = request.method();
  let path = request.uri().path();
  eprintln!("--> {} {}", method, path);
  let method = method.clone();
  let path = path.to_string();
  let start_time = std::time::Instant::now();
  let response = next.run(request).await;
  let duration = start_time.elapsed();
  eprintln!(
    "<-- {} {} {}ms {}",
    method,
    path,
    duration.as_millis(),
    response.status()
  );
  response
}

async fn ws_handler(
  State(sys): State<WordSystem>,
  ws: WebSocketUpgrade,
  user_agent: Option<TypedHeader<headers::UserAgent>>,
  ConnectInfo(addr): ConnectInfo<SocketAddr>,
) -> impl IntoResponse {
  let user_id = UserId::from_uuid(Uuid::new_v4());
  let user_agent = if let Some(TypedHeader(user_agent)) = user_agent {
    user_agent.to_compact_string()
  } else {
    CompactString::new("unknown user agent")
  };
  let addr = addr.to_compact_string();
  println!(">>>> join {user_id}, addr={addr:?}, ua={user_agent:?}");
  ws.on_upgrade(move |socket| handle_socket(sys, user_id, socket))
}

/// Actual websocket statemachine (one will be spawned per connection)
async fn handle_socket(sys: WordSystem, user_id: UserId, mut socket: WebSocket) {
  // send a ping (unsupported by some browsers) just to kick things off and get a response
  if socket
    .send(Message::Ping(user_id.into_uuid().into_bytes().to_vec()))
    .await
    .is_ok()
  {
    println!("PING {user_id}");
  } else {
    println!("#### {user_id} ping error");
    // no Error here since the only thing we can do is to close the connection.
    // If we can not send messages, there is no way to salvage the statemachine anyway.
    return;
  }

  // receive single message from a client (we can either receive or send with socket).
  // this will likely be the Pong for our Ping or a hello message from client.
  // waiting for message from a client will block this task, but will not block other client's
  // connections.
  if let Some(msg) = socket.recv().await {
    if let Ok(msg) = msg {
      match msg {
        Message::Pong(payload) => {
          if payload == user_id.into_uuid().into_bytes().to_vec() {
            println!("PONG {user_id}: ACCEPT");
          } else {
            println!(
              "#### {user_id} pong error: ping {:?}, pong {payload:?}",
              user_id.into_uuid().into_bytes().to_vec()
            );
            return;
          }
        }
        m => {
          println!("#### {user_id} pong error: response={m:?}");
          return;
        }
      }
    } else {
      println!("#### {user_id} pong error: early disconnection");
      return;
    }
  }

  let (game_sender, mut game_recv) = mpsc::unbounded_channel::<Event>();
  let (mut ws_sender, mut ws_receiver) = socket.split();

  // read internal game events, write to socket
  let mut send_task: JoinHandle<u64> = tokio::spawn({
    let sys = sys.clone();
    async move {
      let mut send_count: u64 = 0;
      sys.game.create_user(user_id, game_sender).await;
      while let Some(ev) = game_recv.recv().await {
        let ev = match serde_json::to_string(&ev) {
          Ok(ev) => ev,
          Err(e) => {
            println!("#### {user_id} encode_error {ev:?}: {}", DisplayErrorChain(&e));
            break;
          }
        };
        send_count += 1;
        match ws_sender.send(Message::Text(ev.clone())).await {
          Ok(ev) => ev,
          Err(e) => {
            println!("#### {user_id} send_error {ev:?}: {}", DisplayErrorChain(&e));
            break;
          }
        }
      }
      send_count
    }
  });

  // read socket, write internal game events
  let mut recv_task = tokio::spawn(async move {
    let mut cnt = 0;
    while let Some(Ok(Message::Text(msg))) = ws_receiver.next().await {
      cnt += 1;
      let cmd: Command = match serde_json::from_str(&msg) {
        Ok(cmd) => cmd,
        Err(e) => {
          println!("#### {user_id} recv_error {msg:?}: {}", DisplayErrorChain(&e));
          break;
        }
      };
      sys.game.handle(user_id, cmd).await;
      // println!("<--- {user_id} {msg:?}");
      // // print message and break if instructed to do so
      // if process_message(msg, addr).is_break() {
      //   break;
      // }
    }
    cnt
  });

  // If any one of the tasks exit, abort the other.
  tokio::select! {
      rv_a = (&mut send_task) => {
          match rv_a {
              Ok(send_count) => println!("{send_count} messages sent to {user_id}"),
              Err(a) => println!("Error sending messages {a:?}")
          }
          recv_task.abort();
      },
      rv_b = (&mut recv_task) => {
          match rv_b {
              Ok(b) => println!("Received {b} messages"),
              Err(b) => println!("Error receiving messages {b:?}")
          }
          send_task.abort();
      }
  }

  println!("<<<< {user_id}");
}

pub static STATIC_FILES: Dir = include_dir!("./frontend/dist/frontend/browser/");

async fn fallback(uri: Uri) -> (StatusCode, [(header::HeaderName, &'static str); 2], Vec<u8>) {
  const INDEX: &str = "index.html";

  let path = uri.path();
  let path = path.strip_prefix('/').unwrap_or(path);
  let file = if path == INDEX {
    None
  } else {
    STATIC_FILES.get_file(path)
  };
  let (media_type, file) = match file {
    None => {
      let index = match STATIC_FILES.get_file(INDEX) {
        None => {
          eprintln!("<-> GET {} 1ms Not Found", uri.path(),);
          return (
            StatusCode::INTERNAL_SERVER_ERROR,
            [
              (header::X_CONTENT_TYPE_OPTIONS, "nosniff"),
              (header::CONTENT_TYPE, "text/plain; charset=utf-8"),
            ],
            "Not Found".to_string().into_bytes(),
          );
        }
        Some(index) => index,
      };
      ("text/html; charset=utf-8", index)
    }
    Some(file) => {
      // See <https://developer.mozilla.org/en-US/docs/Web/HTTP/Basics_of_HTTP/MIME_types/Common_types>
      let extension = match path.rfind('.').map(|idx| &path[idx..]) {
        Some(".css") => "text/css; charset=utf-8",
        Some(".gif") => "image/gif",
        Some(".ico") => "image/vnd.microsoft.icon",
        Some(".jpg") => "image/jpeg",
        Some(".jpeg") => "image/jpeg",
        Some(".js") => "text/javascript; charset=utf-8",
        Some(".png") => "image/png",
        Some(".svg") => "image/svg+xml; charset=utf-8",
        Some(".woff2") => "font/woff2",
        ext => {
          if let Some(ext) = ext {
            eprintln!("unknown file extension: {ext}");
          }
          "application/octet-stream"
        }
      };
      (extension, file)
    }
  };
  eprintln!("<-> GET {} 1ms OK", uri.path(),);
  (
    StatusCode::OK,
    [
      (header::X_CONTENT_TYPE_OPTIONS, "nosniff"),
      (header::CONTENT_TYPE, media_type),
    ],
    file.contents().to_vec(),
  )
}
