use crate::game::Game;
use std::sync::Arc;

#[derive(Debug, Clone)]
pub struct WordSystem {
  pub game: Arc<Game>,
}
