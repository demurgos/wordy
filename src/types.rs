use core::fmt;
#[cfg(feature = "serde")]
use eternaltwin_serde_tools::{Deserialize, Serialize};

/// A weakly typed error, used to wrap external errors
#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash)]
#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
pub struct WeakError(String);

impl WeakError {
  pub fn new<S: fmt::Display>(message: S) -> Self {
    Self(message.to_string())
  }

  pub fn wrap<E: std::error::Error>(error: E) -> Self {
    Self::new(format!("{:#}", DisplayErrorChain(&error)))
  }

  pub fn wrap_any(error: AnyError) -> Self {
    Self::new(format!("{:#}", DisplayErrorChain(&*error)))
  }
}

impl fmt::Display for WeakError {
  fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
    f.write_str(self.0.as_str())
  }
}

impl std::error::Error for WeakError {}

pub type AnyError = Box<dyn ::std::error::Error + Send + Sync + 'static>;

pub fn to_any_error<E: ::std::error::Error + Send + Sync + 'static>(e: E) -> AnyError {
  Box::new(e)
}

pub trait DisplayErrorChainExt: std::error::Error {
  fn display_chain(&self) -> DisplayErrorChain<'_, Self> {
    DisplayErrorChain(self)
  }
}

impl<E> DisplayErrorChainExt for E where E: std::error::Error + ?Sized {}

/// Helper struct to print errors with their source chain.
pub struct DisplayErrorChain<'e, E>(pub &'e E)
where
  E: std::error::Error + ?Sized;

impl<'e, E> fmt::Display for DisplayErrorChain<'e, E>
where
  E: std::error::Error + ?Sized,
{
  fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
    fmt::Display::fmt(&self.0, f)?;
    for e in core::iter::successors(self.0.source(), |e| e.source()) {
      f.write_str(": ")?;
      fmt::Display::fmt(e, f)?;
    }
    Ok(())
  }
}

#[macro_export]
macro_rules! declare_decimal_id {
  (
    $(#[$struct_meta:meta])*
    $struct_vis:vis struct $struct_name:ident($struct_ty:ty);
    $(#[$err_meta:meta])*
    $err_vis:vis type ParseError = $err_name:ident;
    const BOUNDS = $bounds:expr;
  ) => {
    $(#[$err_meta:meta])*
    #[derive(Debug)]
    pub struct $err_name(());

    impl ::std::fmt::Display for $err_name {
      fn fmt(&self, fmt: &mut ::std::fmt::Formatter) -> ::std::fmt::Result {
        ::std::fmt::Display::fmt(concat!("Invalid ", stringify!($struct_name)), fmt)
      }
    }

    impl ::std::error::Error for $err_name {}

    $(#[$struct_meta])*
    #[derive(Debug, Copy, Clone, Hash, Eq, PartialEq, Ord, PartialOrd)]
    $struct_vis struct $struct_name($struct_ty);

    impl $struct_name {
      /// Calls `f` with the string representation of this id as an argument.
      #[inline]
      $struct_vis fn with_str<T>(self, f: impl FnOnce(&str) -> T) -> T {
        let mut buf = ::itoa::Buffer::new();
        f(buf.format(self.0))
      }

      /// Constructs a id with checks.
      $struct_vis fn new(id: $struct_ty) -> ::std::result::Result<Self, $err_name> {
        if $bounds.contains(&id) {
          Ok(Self(id))
        } else {
          Err($err_name(()))
        }
      }

      /// Constructs a id without checking bounds
      ///
      /// # Safety
      ///
      /// The caller must ensure that the argument is contained in the bounds.
      $struct_vis const unsafe fn new_unchecked(id: $struct_ty) -> Self {
        Self(id)
      }

      /// Return the inner numeric value.
      $struct_vis const fn get(self) -> $struct_ty {
        self.0
      }
    }

    impl ::std::fmt::Display for $struct_name {
      fn fmt(&self, fmt: &mut ::std::fmt::Formatter) -> ::std::fmt::Result {
        ::std::fmt::Display::fmt(&self.0, fmt)
      }
    }

    impl ::std::str::FromStr for $struct_name {
      type Err = $err_name;

      fn from_str(s: &str) -> ::std::result::Result<Self, Self::Err> {
        match s.parse::<$struct_ty>() {
          Ok(id) if $bounds.contains(&id) => Ok(Self(id)),
          _ => Err($err_name(()))
        }
      }
    }

    impl ::std::convert::TryFrom<&str> for $struct_name {
      type Error = $err_name;

      fn try_from(s: &str) ->  ::std::result::Result<Self, Self::Error> {
        s.parse()
      }
    }

    impl ::serde::Serialize for $struct_name {
      fn serialize<S: ::serde::Serializer>(&self, serializer: S) ->  ::std::result::Result<S::Ok, S::Error> {
        self.with_str(|s| ::serde::Serialize::serialize(s, serializer))
      }
    }

    impl<'de> ::serde::Deserialize<'de> for $struct_name {
      fn deserialize<D: ::serde::Deserializer<'de>>(deserializer: D) ->  ::std::result::Result<Self, D::Error> {
        struct SerdeVisitor;
        impl<'de> ::serde::de::Visitor<'de> for SerdeVisitor {
          type Value = $struct_name;

          fn expecting(&self, fmt: &mut ::std::fmt::Formatter) -> std::fmt::Result {
            fmt.write_str("a string representing a decimal id")
          }

          fn visit_str<E: ::serde::de::Error>(self, value: &str) ->  ::std::result::Result<Self::Value, E> {
            value.parse().map_err(E::custom)
          }

          fn visit_i8<E: ::serde::de::Error>(self, value: i8) ->  ::std::result::Result<Self::Value, E> {
            let value: $struct_ty = ::core::convert::TryFrom::try_from(value).map_err(|_| E::custom($err_name(())))?;
            $struct_name::new(value).map_err(E::custom)
          }

          fn visit_i16<E: ::serde::de::Error>(self, value: i16) ->  ::std::result::Result<Self::Value, E> {
            let value: $struct_ty = ::core::convert::TryFrom::try_from(value).map_err(|_| E::custom($err_name(())))?;
            $struct_name::new(value).map_err(E::custom)
          }

          fn visit_i32<E: ::serde::de::Error>(self, value: i32) ->  ::std::result::Result<Self::Value, E> {
            let value: $struct_ty = ::core::convert::TryFrom::try_from(value).map_err(|_| E::custom($err_name(())))?;
            $struct_name::new(value).map_err(E::custom)
          }

          fn visit_i64<E: ::serde::de::Error>(self, value: i64) ->  ::std::result::Result<Self::Value, E> {
            let value: $struct_ty = ::core::convert::TryFrom::try_from(value).map_err(|_| E::custom($err_name(())))?;
            $struct_name::new(value).map_err(E::custom)
          }

          fn visit_i128<E: ::serde::de::Error>(self, value: i128) ->  ::std::result::Result<Self::Value, E> {
            let value: $struct_ty = ::core::convert::TryFrom::try_from(value).map_err(|_| E::custom($err_name(())))?;
            $struct_name::new(value).map_err(E::custom)
          }

          fn visit_u8<E: ::serde::de::Error>(self, value: u8) ->  ::std::result::Result<Self::Value, E> {
            let value: $struct_ty = ::core::convert::TryFrom::try_from(value).map_err(|_| E::custom($err_name(())))?;
            $struct_name::new(value).map_err(E::custom)
          }

          fn visit_u16<E: ::serde::de::Error>(self, value: u16) ->  ::std::result::Result<Self::Value, E> {
            let value: $struct_ty = ::core::convert::TryFrom::try_from(value).map_err(|_| E::custom($err_name(())))?;
            $struct_name::new(value).map_err(E::custom)
          }

          fn visit_u32<E: ::serde::de::Error>(self, value: u32) ->  ::std::result::Result<Self::Value, E> {
            let value: $struct_ty = ::core::convert::TryFrom::try_from(value).map_err(|_| E::custom($err_name(())))?;
            $struct_name::new(value).map_err(E::custom)
          }

          fn visit_u64<E: ::serde::de::Error>(self, value: u64) ->  ::std::result::Result<Self::Value, E> {
            let value: $struct_ty = ::core::convert::TryFrom::try_from(value).map_err(|_| E::custom($err_name(())))?;
            $struct_name::new(value).map_err(E::custom)
          }

          fn visit_u128<E: ::serde::de::Error>(self, value: u128) ->  ::std::result::Result<Self::Value, E> {
            let value: $struct_ty = ::core::convert::TryFrom::try_from(value).map_err(|_| E::custom($err_name(())))?;
            $struct_name::new(value).map_err(E::custom)
          }

          fn visit_f64<E: ::serde::de::Error>(self, value: f64) ->  ::std::result::Result<Self::Value, E> {
            let value: $struct_ty = value as $struct_ty;
            $struct_name::new(value).map_err(E::custom)
          }
        }

        deserializer.deserialize_any(SerdeVisitor)
      }
    }
  };
}

#[macro_export]
macro_rules! declare_new_string {
  (
    $(#[$struct_meta:meta])*
    $struct_vis:vis struct $struct_name:ident(String);
    $(#[$err_meta:meta])*
    $err_vis:vis type ParseError = $err_name:ident;
    const PATTERN = $pattern:expr;
  ) => {
    $(#[$err_meta:meta])*
    #[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash)]
    pub struct $err_name(());

    impl ::std::fmt::Display for $err_name {
      fn fmt(&self, fmt: &mut ::std::fmt::Formatter) -> ::std::fmt::Result {
        ::std::fmt::Display::fmt(concat!("Invalid ", stringify!($struct_name)), fmt)
      }
    }

    impl ::std::error::Error for $err_name {}

    $(#[$struct_meta])*
    #[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash)]
    $struct_vis struct $struct_name(String);

    impl $struct_name {
      $struct_vis fn pattern() -> &'static ::regex::Regex {
        #[allow(clippy::trivial_regex)]
        static PATTERN: ::once_cell::sync::Lazy<::regex::Regex> = ::once_cell::sync::Lazy::new(||
          ::regex::Regex::new($pattern).unwrap()
        );
        &*PATTERN
      }

      #[inline]
      $struct_vis fn as_str(&self) -> &str {
        &self.0
      }
    }

    impl ::std::fmt::Display for $struct_name {
      fn fmt(&self, fmt: &mut ::std::fmt::Formatter) -> ::std::fmt::Result {
        ::std::fmt::Display::fmt(&self.0, fmt)
      }
    }

    impl ::std::str::FromStr for $struct_name {
      type Err = $err_name;

      fn from_str(s: &str) ->  ::std::result::Result<Self, Self::Err> {
        if Self::pattern().is_match(&s) {
          Ok(Self(s.to_string()))
        } else {
          Err($err_name(()))
        }
      }
    }

    impl ::std::convert::TryFrom<&str> for $struct_name {
      type Error = $err_name;

      fn try_from(s: &str) ->  ::std::result::Result<Self, Self::Error> {
        s.parse()
      }
    }

    impl ::serde::Serialize for $struct_name {
      fn serialize<S: ::serde::Serializer>(&self, serializer: S) ->  ::std::result::Result<S::Ok, S::Error> {
        ::serde::Serialize::serialize(self.as_str(), serializer)
      }
    }

    impl<'de> ::serde::Deserialize<'de> for $struct_name {
      fn deserialize<D: ::serde::Deserializer<'de>>(deserializer: D) ->  ::std::result::Result<Self, D::Error> {
        struct SerdeVisitor;
        impl<'de> ::serde::de::Visitor<'de> for SerdeVisitor {
          type Value = $struct_name;

          fn expecting(&self, fmt: &mut ::std::fmt::Formatter) -> std::fmt::Result {
            fmt.write_str(concat!("a string for a valid ", stringify!($struct_name)))
          }

          fn visit_str<E: ::serde::de::Error>(self, value: &str) ->  ::std::result::Result<Self::Value, E> {
            value.parse().map_err(E::custom)
          }
        }

        deserializer.deserialize_str(SerdeVisitor)
      }
    }
  };
}

#[macro_export]
macro_rules! declare_new_int {
  (
    $(#[$struct_meta:meta])*
    $struct_vis:vis struct $struct_name:ident($struct_ty:ty);
    $(#[$err_meta:meta])*
    $err_vis:vis type RangeError = $err_name:ident;
    const BOUNDS = $bounds:expr;
  ) => {
    $(#[$err_meta:meta])*
    #[derive(Debug)]
    pub struct $err_name(());

    impl ::std::fmt::Display for $err_name {
      fn fmt(&self, fmt: &mut ::std::fmt::Formatter) -> ::std::fmt::Result {
        ::std::fmt::Display::fmt(concat!("Invalid ", stringify!($struct_name)), fmt)
      }
    }

    impl ::std::error::Error for $err_name {}

    $(#[$struct_meta])*
    #[derive(Debug, Copy, Clone, Hash, Eq, PartialEq, Ord, PartialOrd)]
    $struct_vis struct $struct_name($struct_ty);

    impl $struct_name {
      /// Constructs the integer if the given value matches the bounds
      $struct_vis fn new(n: $struct_ty) -> Result<Self, $err_name> {
        if $bounds.contains(&n) {
          Ok(Self(n))
        } else {
          Err($err_name(()))
        }
      }

      /// Constructs the value without checking its bounds
      ///
      /// # Safety
      ///
      /// The caller must ensure that the argument is contained in the bounds.
      $struct_vis const unsafe fn new_unchecked(n: $struct_ty) -> Self {
        Self(n)
      }

      /// Returns the inner value as a primitive type
      $struct_vis const fn get(self) -> $struct_ty {
        self.0
      }

      /// Constructs the integer from an u64 if the given value matches the bounds
      $struct_vis fn from_u64(n: u64) -> Result<Self, $err_name> {
        use core::convert::TryInto;
        let value: $struct_ty = n.try_into().unwrap();
        Self::new(value)
      }

      /// Constructs the integer from an i64 if the given value matches the bounds
      $struct_vis fn from_i64(n: i64) -> Result<Self, $err_name> {
        use core::convert::TryInto;
        let value: $struct_ty = n.try_into().unwrap();
        Self::new(value)
      }
    }

    impl ::std::fmt::Display for $struct_name {
      fn fmt(&self, fmt: &mut ::std::fmt::Formatter) -> ::std::fmt::Result {
        ::std::fmt::Display::fmt(&self.0, fmt)
      }
    }

    impl ::std::convert::TryFrom<i8> for $struct_name {
      type Error = $err_name;

      fn try_from(n: i8) ->  ::std::result::Result<Self, Self::Error> {
        Self::from_i64(i64::from(n))
      }
    }

    impl ::std::convert::TryFrom<i16> for $struct_name {
      type Error = $err_name;

      fn try_from(n: i16) ->  ::std::result::Result<Self, Self::Error> {
        Self::from_i64(i64::from(n))
      }
    }

    impl ::std::convert::TryFrom<i32> for $struct_name {
      type Error = $err_name;

      fn try_from(n: i32) ->  ::std::result::Result<Self, Self::Error> {
        Self::from_i64(i64::from(n))
      }
    }

    impl ::std::convert::TryFrom<i64> for $struct_name {
      type Error = $err_name;

      fn try_from(n: i64) ->  ::std::result::Result<Self, Self::Error> {
        Self::from_i64(i64::from(n))
      }
    }

    impl ::serde::Serialize for $struct_name {
      fn serialize<S: ::serde::Serializer>(&self, serializer: S) ->  ::std::result::Result<S::Ok, S::Error> {
        ::serde::Serialize::serialize(&self.get(), serializer)
      }
    }

    impl<'de> ::serde::Deserialize<'de> for $struct_name {
      fn deserialize<D: ::serde::Deserializer<'de>>(deserializer: D) ->  ::std::result::Result<Self, D::Error> {
        struct SerdeVisitor;
        impl<'de> ::serde::de::Visitor<'de> for SerdeVisitor {
          type Value = $struct_name;

          fn expecting(&self, fmt: &mut ::std::fmt::Formatter) -> std::fmt::Result {
            fmt.write_str("a valid integer")
          }

          fn visit_i64<E: ::serde::de::Error>(self, value: i64) ->  ::std::result::Result<Self::Value, E> {
            $struct_name::from_i64(value).map_err(E::custom)
          }

          fn visit_u64<E: ::serde::de::Error>(self, value: u64) ->  ::std::result::Result<Self::Value, E> {
            $struct_name::from_u64(value).map_err(E::custom)
          }
        }

        deserializer.deserialize_i64(SerdeVisitor)
      }
    }
  };
}

#[macro_export]
macro_rules! declare_new_uuid {
  (
    $(#[$struct_meta:meta])*
    $struct_vis:vis struct $struct_name:ident(Uuid);
    $(#[$err_meta:meta])*
    $err_vis:vis type ParseError = $err_name:ident;
    $(const SQL_NAME = $sql_name:literal;)?
  ) => {
    $(#[$err_meta:meta])*
    #[derive(Debug)]
    pub struct $err_name(());

    impl ::std::fmt::Display for $err_name {
      fn fmt(&self, fmt: &mut ::std::fmt::Formatter) -> ::std::fmt::Result {
        ::std::fmt::Display::fmt(concat!("Invalid ", stringify!($struct_name)), fmt)
      }
    }

    impl ::std::error::Error for $err_name {}

    $(#[$struct_meta])*
    #[derive(Debug, Copy, Clone, Hash, Eq, PartialEq, Ord, PartialOrd)]
    $struct_vis struct $struct_name(::uuid::Uuid);

    impl $struct_name {
      $struct_vis const fn from_uuid(inner: ::uuid::Uuid) -> Self {
        Self(inner)
      }

      $struct_vis const fn from_u128(inner: u128) -> Self {
        Self::from_uuid(::uuid::Uuid::from_u128(inner))
      }

      $struct_vis const fn into_uuid(self) -> ::uuid::Uuid {
        self.0
      }

      $struct_vis fn to_hex(&self) -> String {
        self.0.simple().to_string()
      }
    }

    impl ::std::fmt::Display for $struct_name {
      fn fmt(&self, fmt: &mut ::std::fmt::Formatter) -> ::std::fmt::Result {
        ::std::fmt::Display::fmt(&self.0, fmt)
      }
    }

    impl ::std::str::FromStr for $struct_name {
      type Err = $err_name;

      fn from_str(s: &str) ->  ::std::result::Result<Self, Self::Err> {
        ::uuid::Uuid::parse_str(s).map(Self).map_err(|_| $err_name(()))
      }
    }

    impl ::std::convert::TryFrom<&str> for $struct_name {
      type Error = $err_name;

      fn try_from(s: &str) ->  ::std::result::Result<Self, Self::Error> {
        s.parse()
      }
    }

    impl ::std::convert::From<::uuid::Uuid> for $struct_name {
      fn from(inner: ::uuid::Uuid) ->  Self {
        Self::from_uuid(inner)
      }
    }

    impl ::serde::Serialize for $struct_name {
      fn serialize<S: ::serde::Serializer>(&self, serializer: S) ->  ::std::result::Result<S::Ok, S::Error> {
        ::serde::Serialize::serialize(&self.0, serializer)
      }
    }


    impl<'de> ::serde::Deserialize<'de> for $struct_name {
      fn deserialize<D: ::serde::Deserializer<'de>>(deserializer: D) ->  ::std::result::Result<Self, D::Error> {
        Ok(Self::from_uuid(::uuid::Uuid::deserialize(deserializer)?))
      }
    }
  };
}

#[macro_export]
macro_rules! declare_new_enum {
  (
    $(#[$enum_meta:meta])*
    $enum_vis:vis enum $enum_name:ident {
      $(
        #[str($variant_str:expr)]
        $(#[$variant_meta:meta])*
        $variant_name:ident,
      )*
    }
    $(#[$err_meta:meta])*
    $err_vis:vis type ParseError = $err_name:ident;
  ) => {
    $(#[$err_meta:meta])*
    #[derive(Debug)]
    pub struct $err_name(());

    impl ::std::fmt::Display for $err_name {
      fn fmt(&self, fmt: &mut ::std::fmt::Formatter) -> ::std::fmt::Result {
        ::std::fmt::Display::fmt(concat!("Invalid ", stringify!($enum_name)), fmt)
      }
    }

    impl ::std::error::Error for $err_name {}

    $(#[$enum_meta])*
    #[derive(Debug, Copy, Clone, Hash, Eq, PartialEq, Ord, PartialOrd)]
    $enum_vis enum $enum_name {
      $(
        $(#[$variant_meta])*
        $variant_name,
      )*
    }

    impl $enum_name {
      $enum_vis const CARDINALITY: usize = [$(Self::$variant_name,)*].len();

      $enum_vis const ALL: [Self; Self::CARDINALITY] = [
          $(Self::$variant_name,)*
      ];

      #[inline]
      $enum_vis const fn as_str(self) -> &'static str {
        match self {
          $(Self::$variant_name => $variant_str,)*
        }
      }

      $enum_vis fn iter() -> std::array::IntoIter<Self, { Self::CARDINALITY }> {
        Self::ALL.into_iter()
      }
    }

    impl ::std::fmt::Display for $enum_name {
      fn fmt(&self, fmt: &mut ::std::fmt::Formatter) -> ::std::fmt::Result {
        ::std::fmt::Display::fmt(&self.as_str(), fmt)
      }
    }

    impl ::std::str::FromStr for $enum_name {
      type Err = $err_name;

      fn from_str(s: &str) ->  ::std::result::Result<Self, Self::Err> {
        match s {
          $($variant_str => Ok(Self::$variant_name),)*
          _ => Err($err_name(())),
        }
      }
    }

    impl ::std::convert::TryFrom<&str> for $enum_name {
      type Error = $err_name;

      fn try_from(s: &str) ->  ::std::result::Result<Self, Self::Error> {
        s.parse()
      }
    }


    impl ::serde::Serialize for $enum_name {
      fn serialize<S: ::serde::Serializer>(&self, serializer: S) ->  ::std::result::Result<S::Ok, S::Error> {
        ::serde::Serialize::serialize(self.as_str(), serializer)
      }
    }


    impl<'de> ::serde::Deserialize<'de> for $enum_name {
      fn deserialize<D: ::serde::Deserializer<'de>>(deserializer: D) ->  ::std::result::Result<Self, D::Error> {
        struct SerdeVisitor;
        impl<'de> ::serde::de::Visitor<'de> for SerdeVisitor {
          type Value = $enum_name;

          fn expecting(&self, fmt: &mut ::std::fmt::Formatter) -> std::fmt::Result {
            fmt.write_str(concat!("a string for a valid ", stringify!($enum_name)))
          }

          fn visit_str<E: ::serde::de::Error>(self, value: &str) ->  ::std::result::Result<Self::Value, E> {
            value.parse().map_err(E::custom)
          }
        }

        deserializer.deserialize_str(SerdeVisitor)
      }
    }
  };
}
