use compact_str::CompactString;
use rand::rngs::ThreadRng;

#[derive(Debug)]
pub struct WordList {
  list: Vec<CompactString>,
}

impl WordList {
  pub fn embedded() -> Self {
    let list = include_str!("./list.pl.txt");
    let list = Vec::from_iter(
      list
        .split('\n')
        .filter(|s| !s.trim().is_empty())
        .map(CompactString::new),
    );
    Self { list }
  }

  pub fn pick(&self) -> CompactString {
    use rand::seq::SliceRandom;
    let mut rng = ThreadRng::default();
    self.list.as_slice().choose(&mut rng).unwrap().clone()
  }
}
